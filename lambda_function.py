"""
This sample demonstrates a simple skill built with the Amazon Alexa Skills Kit.
The Intent Schema, Custom Slots, and Sample Utterances for this skill, as well
as testing instructions are located at http://amzn.to/1LzFrj6

For additional samples, visit the Alexa Skills Kit Getting Started guide at
http://amzn.to/1LGWsLG
"""

from __future__ import print_function
from random import *
from sys import maxint

# --------------- Helpers that build all of the responses ----------------------

def build_speechlet_response(title, output, reprompt_text, should_end_session):
    return {
        'outputSpeech': {
            'type': 'PlainText',
            'text': output
        },
        'card': {
            'type': 'Simple',
            'title': "SessionSpeechlet - " + title,
            'content': "SessionSpeechlet - " + output
        },
        'reprompt': {
            'outputSpeech': {
                'type': 'PlainText',
                'text': reprompt_text
            }
        },
        'shouldEndSession': should_end_session
    }


def build_response(session_attributes, speechlet_response):
    return {
        'version': '1.0',
        'sessionAttributes': session_attributes,
        'response': speechlet_response
    }


# --------------- Functions that control the skill's behavior ------------------

def get_welcome_response():
    """ If we wanted to initialize the session to have some attributes we could
    add those here
    """

    session_attributes = {}
    card_title = "Welcome"
    speech_output = "Welcome to the Alexa Skills Kit sample. " \
                    "Please tell me your favorite color by saying, " \
                    "my favorite color is red"
    # If the user either does not reply to the welcome message or says something
    # that is not understood, they will be prompted again with this text.
    reprompt_text = "Please tell me your favorite color by saying, " \
                    "my favorite color is red."
    should_end_session = False
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))


def handle_session_end_request():
    card_title = "Session Ended"
    speech_output = "Thank you for trying the Alexa Skills Kit sample. " \
                    "Have a nice day! "
    # Setting this to true ends the session and exits the skill.
    should_end_session = True
    return build_response({}, build_speechlet_response(
        card_title, speech_output, None, should_end_session))


def create_course_attributes(course):
    return {"course": course}


def set_course_in_session(intent, session):
    """ Sets the color in the session and prepares the speech to reply to the
    user.
    """

    card_title = intent['name']
    session_attributes = {}
    should_end_session = False

    if 'Course' in intent['slots']:
        course = intent['slots']['Course']['value']
        session_attributes = create_course_attributes(course)
        speech_output = "You have selected " + \
                        course + \
                        ". You can ask me about grades or announcements. "
        reprompt_text = "Ask me about grades or announcements for " + \
                        course + "."
    else:
        speech_output = "You have not selected a class yet." \
                        "Please try again."
        reprompt_text = "Please select a class."
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))


def get_grade(intent, session):
    session_attributes = {}
    reprompt_text = None

    if session.get('attributes', {}) and "course" in session.get('attributes', {}):
        course = session['attributes']['course']
        session_attributes = create_course_attributes(course)
        speech_output = "You currently have " + str(round(80+(20*(hash(course)/float(maxint))))) + \
                        " percent in " + course
        should_end_session = False
    else:
        speech_output = "You haven't selected a class yet. " \
                        "Please try again."
        should_end_session = False


    return build_response(session_attributes, build_speechlet_response(
        intent['name'], speech_output, reprompt_text, should_end_session))

def get_last_grade(intent, session):
    session_attributes = {}
    reprompt_text = None
    assignment_list = ["Homework 3","Homework 2","Exam 2","Lab 8","Writing assignment 1","Final Project"]
    index = randrange(0,6,1)

    if session.get('attributes', {}) and "course" in session.get('attributes', {}):
        course = session['attributes']['course']
        session_attributes = create_course_attributes(course)
        speech_output = "You got " + str(round(60+abs(20*(hash(course)/float(maxint)))+abs(20*(hash(assignment_list[index])/float(maxint))))) + \
                        " percent on " + assignment_list[index]
        should_end_session = False
    else:
        speech_output = "You haven't selected a class yet. " \
                        "Please try again."
        should_end_session = False


    return build_response(session_attributes, build_speechlet_response(
        intent['name'], speech_output, reprompt_text, should_end_session))

def get_announcement(intent, session):
    session_attributes = {}
    reprompt_text = None
    announcement_list1 = ["Dear ","Today I will release the ","","","",""]
    announcement_list2 = [" students , I will have office hours today until 11:30."," test scores."," Midterm 2 photo id required allowed one page of notes allowed."," ,All books must be returned by the end of the week",", Quiz 3 will be next week"," Tests will be handed back next Thursday"]
    index = randrange(0,6,1)
    
    if session.get('attributes', {}) and "course" in session.get('attributes', {}):
        course = session['attributes']['course']
        session_attributes = create_course_attributes(course)
        speech_output = announcement_list1[index] + course + announcement_list2[index]
        should_end_session = False
    else:
        speech_output = "You haven't selected a class yet. " \
                        "Please try again."
        should_end_session = False


    return build_response(session_attributes, build_speechlet_response(
        intent['name'], speech_output, reprompt_text, should_end_session))

# --------------- Events ------------------

def on_session_started(session_started_request, session):
    """ Called when the session starts """

    print("on_session_started requestId=" + session_started_request['requestId']
          + ", sessionId=" + session['sessionId'])


def on_launch(launch_request, session):
    """ Called when the user launches the skill without specifying what they
    want
    """

    print("on_launch requestId=" + launch_request['requestId'] +
          ", sessionId=" + session['sessionId'])
    # Dispatch to your skill's launch
    return get_welcome_response()


def on_intent(intent_request, session):
    """ Called when the user specifies an intent for this skill """

    print("on_intent requestId=" + intent_request['requestId'] +
          ", sessionId=" + session['sessionId'])

    intent = intent_request['intent']
    intent_name = intent_request['intent']['name']

    # Dispatch to your skill's intent handlers
    if intent_name == "CourseIntent":
        return set_course_in_session(intent, session)
    elif intent_name == "GradeIntent":
        return get_grade(intent, session)
    elif intent_name == "LastGradedIntent":
        return get_last_grade(intent, session)
    elif intent_name == "AnnouncementIntent":
        return get_announcement(intent, session)
    elif intent_name == "AMAZON.HelpIntent":
        return get_welcome_response()
    elif intent_name == "AMAZON.CancelIntent" or intent_name == "AMAZON.StopIntent":
        return handle_session_end_request()
    else:
        raise ValueError("Invalid intent")


def on_session_ended(session_ended_request, session):
    """ Called when the user ends the session.

    Is not called when the skill returns should_end_session=true
    """
    print("on_session_ended requestId=" + session_ended_request['requestId'] +
          ", sessionId=" + session['sessionId'])
    # add cleanup logic here


# --------------- Main handler ------------------

def lambda_handler(event, context):
    """ Route the incoming request based on type (LaunchRequest, IntentRequest,
    etc.) The JSON body of the request is provided in the event parameter.
    """
    print("event.session.application.applicationId=" +
          event['session']['application']['applicationId'])

    """
    Uncomment this if statement and populate with your skill's application ID to
    prevent someone else from configuring a skill that sends requests to this
    function.
    """
    # if (event['session']['application']['applicationId'] !=
    #         "amzn1.echo-sdk-ams.app.[unique-value-here]"):
    #     raise ValueError("Invalid Application ID")

    if event['session']['new']:
        on_session_started({'requestId': event['request']['requestId']},
                           event['session'])

    if event['request']['type'] == "LaunchRequest":
        return on_launch(event['request'], event['session'])
    elif event['request']['type'] == "IntentRequest":
        return on_intent(event['request'], event['session'])
    elif event['request']['type'] == "SessionEndedRequest":
        return on_session_ended(event['request'], event['session'])
